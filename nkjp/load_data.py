#! /usr/bin/env python
import sqlite3


def load_data(filename1, filename2, filename3, database):
    conn = sqlite3.connect(database)
    c = conn.cursor()

    #c.execute(''' CREATE TABLE unigrams (txt1 text, count real) ''')

    #with open(filename1, 'r') as f:
    #    for l in f:
    #        s = l.split()
    #        c.execute("INSERT INTO unigrams VALUES (?,?)", (s[1], s[0]))

    c.execute("CREATE INDEX txt_idx ON unigrams (txt1)")


    # c.execute(''' CREATE TABLE bigrams (txt1 text, txt2 text, count real) ''')

    # with open(filename2, 'r') as f:
    #     for l in f:
    #         s = l.split()
    #         c.execute("INSERT INTO bigrams VALUES (?,?,?)", (s[1], s[2], s[0]))

    # c.execute(''' CREATE TABLE trigrams (txt1 text, txt2 text, txt3 text, count real) ''')
    # with open(filename3, 'r') as f:
    #     for l in f:
    #         s = l.split()
    #         c.execute("INSERT INTO trigrams VALUES (?,?,?, ?)", (s[1], s[2], s[3], s[0]))

    conn.commit()


def main():
    load_data("1grams", "2grams", "3grams", "2grams.db")


if __name__ == "__main__":
    main()
