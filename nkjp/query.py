#! /usr/bin/env python
import sqlite3
import random


def _get_cursor():
    conn = sqlite3.connect("/home/zeroos/uni/pjn/nkjp/2grams.db")
    return conn.cursor()


def get_1gram(txt):
    c = _get_cursor()
    if txt[0] == '-':
        r = c.execute(''' SELECT * FROM unigrams WHERE (txt1 LIKE '%{}') '''.format(txt[1:]))
    else:
        r = c.execute(''' SELECT * FROM unigrams WHERE (txt1 = ?) ''', (txt,))
    return r.fetchone()


def count_all_1gram():
    return 246153378  # cache
    c = _get_cursor()
    r = c.execute(''' SELECT sum(count) FROM unigrams ''')
    return r.fetchone()[0]


def count_all_2gram(txt=None, txt2=None):
    return 246110034  # cache
    c = _get_cursor()
    if txt is None:
        r = c.execute(''' SELECT sum(count) FROM bigrams ''')
    elif txt2 is None:
        r = c.execute(''' SELECT sum(count) FROM bigrams WHERE (txt1 = ?) ''', (txt,))
    else:
        r = c.execute(''' SELECT sum(count) FROM bigrams WHERE (txt1 = ? AND txt2 = ?) ''',
                      (txt, txt2))
    return r.fetchone()[0]




def get_random_2gram():
    return get_random("bigrams")


def get_random_3gram():
    return get_random("trigrams")


def get_random(table):
    c = _get_cursor()
    r = c.execute('SELECT max(_ROWID_) FROM {}'.format(table))
    max_id = r.fetchone()[0]
    rid = random.randint(0, max_id)
    r = c.execute('SELECT * FROM {} WHERE (_ROWID_ = ?) LIMIT 1'.format(table),
                  (rid,))
    return r.fetchone()


def get_all(table, *args):
    if table == "bigrams":
        return get_all_2gram(*args)
    elif table == "trigrams":
        return get_all_3gram(*args)


def get_all_2gram(txt):
    c = _get_cursor()
    r = c.execute(''' SELECT * FROM bigrams WHERE (txt1 = ?) ''', (txt,))
    return r.fetchall()


def get_all_3gram(txt1, txt2):
    c = _get_cursor()
    r = c.execute('SELECT * FROM trigrams WHERE (txt1 = ? AND txt2 = ?)',
                  (txt1, txt2))
    return r.fetchall()


def main():
    pass

if __name__ == "__main__":
    main()
