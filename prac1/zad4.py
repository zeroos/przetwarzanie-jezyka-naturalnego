#! /usr/bin/env python
import sys
from query import _get_cursor


def get_where(num, word):
    if word[0] == '-':
        return "txt{} LIKE '%{}'".format(num, word[1:])
    else:
        return "txt{} = '{}'".format(num, word)


def filter_n_grams(table, txt):
    ngram = 2
    c = _get_cursor()
    stxt = txt.lower().split()
    sys.stderr.write(txt+"\n")
    result = []
    for i in range(len(stxt)-ngram+1):
        where = [get_where(n+1, stxt[i+n]) for n in range(ngram)]
        where = ' AND '.join(where)
        query = 'SELECT * FROM {} WHERE ({})'.format(table, where)
        r = c.execute(query).fetchall()
        result.append((int(sum([k[-1] for k in r])),
                       stxt[i],
                       stxt[i+1]))
    return result


def show_result(result):
    for r in result:
        print("{} {} {}".format(*r))


def main(filename, text):
    r = filter_n_grams(filename, text)
    show_result(r)


if __name__ == "__main__":
    try:
        filename = sys.argv[1]
        text = ' '.join(sys.argv[2:])
    except IndexError:
        filename = "bigrams"
        text = "Babuleńka miała dwa rogate -ołki"
    main(filename, text)
