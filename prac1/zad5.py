#! /usr/bin/env python

import sys
import itertools
from zad4 import filter_n_grams
from query import get_1gram, count_all_1gram, count_all_2gram


def word_prob(w1):
    unigram = get_1gram(w1)
    return unigram[1]/count_all_1gram()


def prob(sentence):
    r = filter_n_grams("bigrams", sentence)
    p = word_prob(sentence.split()[0])

    for bigram in r:
        bigram_cnt = max(bigram[0], 1)
        p *= (bigram_cnt/count_all_2gram())/word_prob(bigram[1])

    return p


def permutations_score(text):
    stxt = text.lower().split()
    result = []
    for p in itertools.permutations(stxt):
        sentence = ' '.join(p)
        result.append((prob(sentence), sentence))
    return sorted(result, reverse=True)


def main(text):
    result = permutations_score(text)
    show_result(result)


def show_result(result):
    for r in result:
        print("{} {}".format(*r))

if __name__ == "__main__":
    if len(sys.argv) > 2:
        text = ' '.join(sys.argv[1:])
    else:
        text = "Babuleńka miała dwa rogate koziołki."

    main(text)
