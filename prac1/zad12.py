#! /usr/bin/env python
import random
import query
import sys


def weighted_choice(choices):
   total = sum(w for c, w in choices)
   r = random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w > r:
         return c
      upto += w
   assert False, "Shouldn't get here"

def generate_text(n, table, weighted=True):
    c = query.get_random(table)
    for _ in range(n):
        res = query.get_all(table, *c[1:-1])
        if len(res)==0:
            res = [query.get_random(table)]
            print("\n###")
        print(c[-2], end=" ")
        if not weighted:
            c = random.choice(res)
        else:
            c = weighted_choice([(c, c[-1]) for c in res])
        sys.stdout.flush()



def main():
    generate_text(500, "trigrams", True)


if __name__ == "__main__":
    main()
